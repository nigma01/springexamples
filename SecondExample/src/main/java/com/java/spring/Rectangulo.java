package com.java.spring;

public class Rectangulo {
	
	private String figura;
	private String area;

	public Rectangulo() {
		super();
	}
	
	
	
	public Rectangulo(String figura) {
		super();
		this.figura = figura;
	}



	public void dibujar(){
		System.out.println("Dibujo un: " + figura);
		System.out.println("Area: " + area);
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}

}
