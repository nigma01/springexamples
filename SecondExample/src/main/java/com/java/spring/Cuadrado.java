package com.java.spring;

public class Cuadrado {
	
	private String figura;
	private String area;
	

	public Cuadrado() {
		super();
	}
	
	public Cuadrado(String figura) {
		this.figura = figura;
	}
	
	public void dibujar(){
		System.out.println("Dibujo un: " + figura);
		System.out.println("Area: " + area);
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}
