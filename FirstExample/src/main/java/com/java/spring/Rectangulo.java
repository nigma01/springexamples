package com.java.spring;

public class Rectangulo {
	
	private String figura;

	public Rectangulo() {
		super();
	}
	
	
	
	public Rectangulo(String figura) {
		super();
		this.figura = figura;
	}



	public void dibujar(){
		System.out.println("Dibujo un: " + figura);
	}

}
