/**
 * 
 */
package com.java.spring;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

/**
 * @author David
 *
 */
@SuppressWarnings("deprecation")
public class DibujandoApp {

	/**
	 * 
	 */
	public DibujandoApp() {
		super();
	}

	/**
	 * @param args
	 */

	public static void main(String[] args) {
	
		BeanFactory beanFactory = new XmlBeanFactory(new FileSystemResource("src\\main\\resources\\spring-context.xml"));
		Cuadrado cuadrado = (Cuadrado)beanFactory.getBean("cuadrado");
		cuadrado.dibujar();
		
		Triangulo triangulo = (Triangulo)beanFactory.getBean("triangulo");
		triangulo.dibujar();
		
		Rectangulo rectangulo = (Rectangulo)beanFactory.getBean("rectangulo");
		rectangulo.dibujar();
	}

}
