package com.java.spring;

public class Cuadrado {
	
	private String figura;

	public Cuadrado() {
		super();
	}
	
	public Cuadrado(String figura) {
		this.figura = figura;
	}
	
	public void dibujar(){
		System.out.println("Dibujo un: " + figura);
	}

}
