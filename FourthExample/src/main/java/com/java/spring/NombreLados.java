/**
 * 
 */
package com.java.spring;

/**
 * @author David
 *
 */
public class NombreLados {

	/**
	 * 
	 */
	
	private String lado1;
	private String lado2;
	private String lado3;
	private String lado4;
	
	public NombreLados() {
		// TODO Auto-generated constructor stub
	}

	public NombreLados(String lado1, String lado2, String lado3, String lado4) {
		super();
		this.lado1 = lado1;
		this.lado2 = lado2;
		this.lado3 = lado3;
		this.lado4 = lado4;
	}

	public String getLado1() {
		return lado1;
	}

	public void setLado1(String lado1) {
		this.lado1 = lado1;
	}

	public String getLado2() {
		return lado2;
	}

	public void setLado2(String lado2) {
		this.lado2 = lado2;
	}

	public String getLado3() {
		return lado3;
	}

	public void setLado3(String lado3) {
		this.lado3 = lado3;
	}

	public String getLado4() {
		return lado4;
	}

	public void setLado4(String lado4) {
		this.lado4 = lado4;
	}

}
