package com.java.spring;

import java.util.List;

public class Cuadrado {
	
	private String figura;
	private String area;
	private CalculoArea calculoarea;
	private List<NombreLados> nombreLadosList;
	

	
	
	public CalculoArea getCalculoarea() {
		return calculoarea;
	}

	public void setCalculoarea(CalculoArea calculoarea) {
		this.calculoarea = calculoarea;
	}

	public void dibujar(){
		System.out.println("Dibujo un: " + figura);
		System.out.println("Area: " + area);		
		System.out.println("Calculo Area: " + calculoarea.getL1()*calculoarea.getL1());
		for(NombreLados nombreLados : nombreLadosList){
			System.out.println(nombreLados.getLado1());
			System.out.println(nombreLados.getLado2());
			System.out.println(nombreLados.getLado3());
			System.out.println(nombreLados.getLado4());
			
		}
		
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	
	
	public Cuadrado() {
		super();
	}
	
	public Cuadrado(String figura) {
		this.figura = figura;
	}

	public List<NombreLados> getNombreLadosList() {
		return nombreLadosList;
	}

	public void setNombreLadosList(List<NombreLados> nombreLadosList) {
		this.nombreLadosList = nombreLadosList;
	}

}
