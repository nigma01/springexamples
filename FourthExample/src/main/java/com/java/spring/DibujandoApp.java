/**
 * 
 */
package com.java.spring;

import java.io.File;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

/**
 * @author David
 *
 */
@SuppressWarnings("deprecation")
public class DibujandoApp {

	/**
	 * 
	 */
	public DibujandoApp() {
		super();
	}

	/**
	 * @param args
	 */

	public static void main(String[] args) {
	
	
		ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");

		
		Cuadrado cuadrado = (Cuadrado)context.getBean("cuadrado");
		cuadrado.dibujar();

		
	}

}
