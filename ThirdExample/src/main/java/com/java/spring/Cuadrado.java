package com.java.spring;

public class Cuadrado {
	
	private String figura;
	private String area;
	private CalculoArea calculoarea;
	

	
	
	public CalculoArea getCalculoarea() {
		return calculoarea;
	}

	public void setCalculoarea(CalculoArea calculoarea) {
		this.calculoarea = calculoarea;
	}

	public void dibujar(){
		System.out.println("Dibujo un: " + figura);
		System.out.println("Area: " + area);		
		System.out.println("Calculo Area: " + calculoarea.getL1()*calculoarea.getL1());
		
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	
	
	public Cuadrado() {
		super();
	}
	
	public Cuadrado(String figura) {
		this.figura = figura;
	}

}
